FROM busybox AS builder

ARG S6_OVERLAY_RELEASE=https://github.com/just-containers/s6-overlay/releases/latest/download/s6-overlay-amd64.tar.gz

ENV S6_OVERLAY_RELEASE=${S6_OVERLAY_RELEASE}

ADD ${S6_OVERLAY_RELEASE} /tmp/s6overlay.tar.gz 

RUN mkdir /installroot \ 
    && tar xzf /tmp/s6overlay.tar.gz -C /installroot \
    && rm /tmp/s6overlay.tar.gz 

# Move to latest Clear Linux release
FROM clearlinux/os-core

LABEL maintainer="Pichate Ins cogent[a]cogentwbebworks.com"

ENV S6_OVERLAY_RELEASE=https://github.com/just-containers/s6-overlay/releases/latest/download/s6-overlay-amd64.tar.gz

COPY --from=builder /installroot /
COPY rootfs /

ENTRYPOINT [ "/init" ]
