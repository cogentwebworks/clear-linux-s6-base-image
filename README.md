# Clear Linux* S6-overelay 
[![Codefresh build status]( https://g.codefresh.io/api/badges/pipeline/dearteno/cogentwebworks%2Fbuild-and-promote?key=eyJhbGciOiJIUzI1NiJ9.NWE2ZjZkNmUzNTI3ZDMwMDAxYmEwYWEz.rA9Uk_mv-TfC3D4bt1PDDRIF6z5BQC8kLODyxtLwCdw&type=cf-2)]( https%3A%2F%2Fg.codefresh.io%2Fpipelines%2Fbuild-and-promote%2Fbuilds%3Ffilter%3Dtrigger%3Abuild~Build%3Bpipeline%3A5e526d8aa284e06d202ea963~build-and-promote)

Clear Linux S6-overelay A minimal container that includes the 's6' process supervisor, to implement a (PID-1) 'init' program that handles the lifecycle of spawned processes.

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

The MIT License
License: MIT

**Running code quality checks:**

To ensure the code quality of this project is kept consistent we make use of pre-commit hooks. To install them, run the commands below.

```bash
brew install pre-commit
pre-commit install --install-hooks -t pre-commit -t commit-msg
```
